package ejercicio_Vista;

import javax.swing.JPanel;
import javax.swing.JLayeredPane;
import javax.swing.JLabel;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JList;

public class VistaAsignar extends JPanel {

	public VistaAsignar() {
		setLayout(null);
		
		JLayeredPane layeredPane = new JLayeredPane();
		layeredPane.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		layeredPane.setBounds(10, 175, 430, 114);
		add(layeredPane);
		
		JLabel lblListadoDePersonas = new JLabel("Listado de Personas");
		lblListadoDePersonas.setBounds(10, 11, 113, 14);
		layeredPane.add(lblListadoDePersonas);
		
		JLabel lblListaDeTareas = new JLabel("Listado de Tareas");
		lblListaDeTareas.setBounds(216, 11, 113, 14);
		layeredPane.add(lblListaDeTareas);
		
		JList list = new JList();
		list.setBounds(10, 31, 204, 72);
		layeredPane.add(list);
		
		JList list_1 = new JList();
		list_1.setBounds(216, 31, 204, 72);
		layeredPane.add(list_1);

	}
}
