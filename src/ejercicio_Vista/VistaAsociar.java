package ejercicio_Vista;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JLayeredPane;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import ejercicio_Vista.VistaInsertar;

public class VistaAsociar extends JPanel {

	public VistaAsociar() {
		
		setLayout(null);
		JLayeredPane Panel2 = new JLayeredPane();
		Panel2.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		Panel2.setBounds(10, 112, 430, 57);
		add(Panel2);
		
		JLabel lblAsociarPersonasA = new JLabel("Asociar Personas a Tareas");
		lblAsociarPersonasA.setBounds(10, 11, 139, 14);
		Panel2.add(lblAsociarPersonasA);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(10, 30, 159, 20);
		Panel2.add(comboBox);
		
		JButton btnNewButton = new JButton("< - >");
		btnNewButton.setBounds(179, 29, 72, 23);
		Panel2.add(btnNewButton);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(261, 30, 159, 20);
		Panel2.add(comboBox_1);
		
	}
}
