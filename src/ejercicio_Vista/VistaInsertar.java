package ejercicio_Vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import ejercicio_Vista.VistaAsociar;

public class VistaInsertar extends JFrame {

	public JPanel contentPane;
	public JTextField textField;
	public JTextField textField_1;

	public VistaInsertar() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLayeredPane Panel1 = new JLayeredPane();
		Panel1.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		Panel1.setBounds(10, 11, 414, 84);
		contentPane.add(Panel1);
		
		JLabel lblInsertarNombres = new JLabel("Insertar Nombres");
		lblInsertarNombres.setBounds(10, 11, 100, 14);
		Panel1.add(lblInsertarNombres);
		
		textField = new JTextField();
		textField.setBounds(10, 29, 185, 20);
		Panel1.add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Insertar Personas");
		btnNewButton.setBounds(10, 54, 185, 23);
		Panel1.add(btnNewButton);
		
		JLabel label = new JLabel("Insertar Tareas");
		label.setBounds(219, 11, 100, 14);
		Panel1.add(label);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(219, 29, 185, 20);
		Panel1.add(textField_1);
		
		JButton button = new JButton("Insertar Personas");
		button.setBounds(219, 54, 185, 23);
		Panel1.add(button);
	}
}
